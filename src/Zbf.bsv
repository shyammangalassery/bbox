function Bit#(XLEN) fn_slo_d(Bit#(XLEN) src1,Bit#(XLEN) src2) provisos(Bitwise#(Bit#(XLEN))) ;
    let shamt = src2 & (fromInteger(valueOf(XLEN))-1);
    return signExtend(~(~src1 << shamt));
  endfunction
/*doc:func:The bit field place (bfp) instruction places up to XLEN/2 LSB bits from rs2 into the value in rs1.
The upper bits of rs2 control the length of the bit field and target position.function Bit#(XLEN) fn_bfp(Bit#(XLEN) rs1, Bit#(XLEN) rs2)*/ function Bit#(XLEN) fn_bfp(Bit#(XLEN) rs1, Bit#(XLEN) rs2) provisos(Bitwise#(Bit#(XLEN))) ;
    Bit#(XLEN) cfg = rs2 >> (fromInteger(valueOf(XLEN))/2);
    if ((cfg >> 30) == 2)
    cfg = cfg >> 16;
    let len = (cfg >> 8) & (fromInteger(valueOf(XLEN))/2-1);
    let off = cfg & (fromInteger(valueOf(XLEN)) - 1);
    len = (len>0) ? len : (fromInteger(valueOf(XLEN))/2);
    Bit#(XLEN) mask = fn_slo_d(0, len) << off;
    Bit#(XLEN) data = rs2 << off;
    return (data & mask) | (rs1 & ~mask);
endfunction

`ifdef RV64
/*doc:func:The bit field place (bfp) instruction places up to XLEN/2 LSB bits from rs2 into the value in rs1.
The upper bits of rs2 control the length of the bit field and target position.function Bit#(XLEN) fn_bfp(Bit#(XLEN) rs1, Bit#(XLEN) rs2);for 64 bits*/
function Bit#(XLEN) fn_bfpw(Bit#(XLEN) rs1, Bit#(XLEN) rs2) provisos(Bitwise#(Bit#(XLEN))) ;
    Bit#(64) cfg = rs2 >> (32);
    if ((cfg >> 30) == 2)
    cfg = cfg >> 16;
    let len = (cfg >> 8) & (31);
    let off = cfg & (63);
    len = (len>0) ? len : 32;
    Bit#(64) mask = fn_slo_d(0, len) << off;
    Bit#(64) data = rs2 << off;
    return ((data & mask) | (rs1 & ~mask) ) & 64'h00000000ffffffff;
endfunction
`endif
