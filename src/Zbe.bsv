 /*doc:func:bext collects LSB justified bits to rd from rs1 using extract mask in rs2.*/
  function Bit#(XLEN) fn_bext(Bit#(XLEN) src1,Bit#(XLEN) src2) provisos(Bitwise#(Bit#(XLEN))) ; 
    Bit#(XLEN) x= 0;
    Bit#(TLog#(XLEN)) count = 0;
     /*while(src2!=0)
    begin
     let y = src2 & -src2 ;
      let r =  countZerosLSB(y);
       x[count] = src1[r];
      count = count +1;
     src2 = src2 - y ;
     end
     */
    Integer s = valueOf(XLEN);  
    for (Integer i = 0; i<s; i = i + 1) begin
      if(src2[i] == 1) begin
        x[count] = src1[i];
        count = count + 1;
      end
    end	
    return x;
  endfunction
  /*doc:func:bdep writes LSB justified bits from rs1 to rd using deposit mask in rs2.*/
  function Bit#(XLEN) fn_bdep(Bit#(XLEN) src1,Bit#(XLEN) src2) provisos(Bitwise#(Bit#(XLEN))) ; 
    Bit#(XLEN) x= 0;
    Bit#(TLog#(XLEN)) count = 0;
    for (Integer i = 0; i<valueOf(XLEN); i = i + 1) begin
      if(src2[i] == 1) begin
        x[i] = src1[count];
        count = count + 1;
      end
    end
    return x;
   endfunction
   
  `ifdef RV64
 /*doc:func:bext collects LSB justified bits to rd from rs1 using extract mask in rs2.*/
 function Bit#(XLEN) fn_bextw(Bit#(XLEN) src1,Bit#(XLEN) src2) provisos(Bitwise#(Bit#(XLEN))) ; 
    Bit#(32) x= 0;
    Bit#(TLog#(32)) count = 0;
     /*while(src2!=0)
    begin
     let y = src2 & -src2 ;
      let r =  countZerosLSB(y);
       x[count] = src1[r];
      count = count +1;
     src2 = src2 - y ;
     end
     */
    let src1_32=src1[31:0];
     let src2_32=src2[31:0];
    for (Integer i = 0; i<32; i = i + 1) begin
      if(src2_32[i] == 1) begin
        x[count] = src1_32[i];
        count = count + 1;
      end
    end	
    return signExtend(x) ;
  endfunction
   /*doc:func:bdep writes LSB justified bits from rs1 to rd using deposit mask in rs2.*/
  function Bit#(XLEN) fn_bdepw(Bit#(XLEN) src1,Bit#(XLEN) src2) provisos(Bitwise#(Bit#(XLEN))) ; 
    Bit#(32) x= 0;
    Bit#(TLog#(32)) count = 0;
    let src1_32=src1[31:0];
     let src2_32=src2[31:0];
    for (Integer i = 0; i<32; i = i + 1) begin
      if(src2_32[i] == 1) begin
        x[i] = src1_32[count];
        count = count + 1;
      end
    end
    return signExtend(x);
   endfunction
 `endif
  

