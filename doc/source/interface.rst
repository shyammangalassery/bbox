BMU Interface
=====================================

BMU is a simple combinatorial block where inputs are applied and outputs are obtained with immediate effect. This can be pictorially represented as

 ![BBox Interface](diagrams/BBox_Interface_v5.png)

As seen from the above interface, 

- **RDY** value at the output is always high,  (Essentially **a Constant**)
- Input **EN** is made high before Input values are fed into BMU (can be observed in the waveform)

*Note:*
The **Bool** Flag represented in the ***mav_putvalue***  represent the legality of the ***XLEN-bit* value** for the (106) instructions specified in the bit-manip draft.