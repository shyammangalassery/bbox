# Bit Manipulation Instruction (BMI) IP

## Simulating BMI Set

To simulate BMI IP first check if *Bluespec* compiler is available and update the dependencies for simulation

```shell
./manager.sh update_deps
```

go to the **bbox** directory where *Makefile* is located & perform

```shell
make help
```

> - **Specify Targeting Architecture** *`Default:= 32`*
>   
>   ```
>   Existing Options for XLEN=32/64
>   ```
>   
> - **Specify Top Module** *`Default:= SIM`*
>   
>   <u>Use SIM for Simulation and FPGA for design Synthesis</u>
>   
>   ```
>   Existing Options for SYNTH=FPGA/SIM
>   ```
>   
> - **Specify Verbosity** *`Default:= 0`*
>   
>   ```
>   Existing Options for VERBOSE=0/1/2
>   ```
>
> - **Specify BMI Extensions** *`Default:= BMI_ALL`*
>   
>   |  |  |
>   | :-------: | :-------: |
>   | ZBB = 1 | ZBP = 1 |
>   | ZBS =1 | ZBA = 1 |
>   | ZBC =1 | ZBR = 1 |
>   | ZBM =1 | ZBT = 1 |
>   | ZBF =1 | ZBE = 1 |
>   

![Bit Manipulation Instructions and its Extension](https://i.postimg.cc/ZnSj5RQt/Screenshot-from-2020-01-29-00-19-44.png)
------


To Generate ***Verilog*** artefacts

```shell
make generate_verilog XLEN=64 SYNTH=FPGA VERBOSE=1
```

To simulate 

```shell
make XLEN=64
```